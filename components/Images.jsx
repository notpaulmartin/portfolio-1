/*
 * File:          Images.jsx
 * Project:       portfolio
 * File Created:  Sunday, 5th January 2020 10:14:57 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Thursday, 9th January 2020 12:32:31 am
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';

const PicturePaul = () => (
  <img
    alt='Paul Martin'
    src='/images/paul.jpg'
    id='img_paul'
    className='sticky' // TODO: remove 'hidden'
  />
);

export default PicturePaul;
