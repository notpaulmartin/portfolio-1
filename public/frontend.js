/*
 * File:          frontend.js
 * Project:       portfolio
 * File Created:  Sunday, 5th January 2020 2:34:26 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Sunday, 12th January 2020 8:07:01 pm
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

function windowSizeCalc() {
  const windowWidth = window.innerWidth;

  if (windowWidth < 525) return 'small';
  else if (windowWidth < 600) return 'mediumS';
  else if (windowWidth < 800) return 'mediumL';
  else if (windowWidth < 1200) return 'largeS';
  else return 'largeL';
}

let windowSize = windowSizeCalc();

function respondTo(func, ...sizes) {
  if (sizes.includes(windowSize)) func();
}

window.addEventListener('resize', () => {
  windowSize = windowSizeCalc();
});

function offsetY(el) {
  const rect = el.getBoundingClientRect();
  const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

  const height = el.clientHeight;

  return {
    top: rect.top + scrollTop,
    middle: rect.top + scrollTop + 0.5 * height
  };
}

// Stick navbar to top
const navbar = document.getElementById('navbar');
const navbarRestingOffset = navbar.offsetTop;

window.addEventListener('scroll', () => {
  if (window.pageYOffset >= navbarRestingOffset) {
    navbar.classList.add('sticky');
  } else {
    navbar.classList.remove('sticky');
  }
});

// sticky profile picture
const aboutSection = document.getElementById('about');
const profilePicture = document.getElementById('img_paul');

window.addEventListener('scroll', () => {
  respondTo(() => {
    // define key coordinates
    const aboutSectionMiddleY = offsetY(aboutSection).middle;
    const windowMiddleY = window.pageYOffset + 0.5 * window.innerHeight;

    if (windowMiddleY <= aboutSectionMiddleY) {
      profilePicture.classList.add('sticky');
      profilePicture.style.top = `50%`;
    } else {
      profilePicture.classList.remove('sticky');
      profilePicture.style.top = `${offsetY(aboutSection).middle -
        0.5 * profilePicture.clientHeight}px`;
    }
  }, 'largeL');
  respondTo(
    () => {
      profilePicture.style = '';
    },
    'small',
    'mediumS',
    'mediumL',
    'largeS'
  );
});
