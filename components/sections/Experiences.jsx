/*
 * File:          Experiences.jsx
 * Project:       portfolio
 * File Created:  Monday, 6th January 2020 6:26:28 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Sunday, 12th January 2020 8:22:38 pm
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';
import PropTypes from 'prop-types';

const Experience = ({ timespan, name, imgUrl, children }) => {
  let img;
  if (imgUrl) img = <img alt={name} src={imgUrl} />;
  else img = '';

  return (
    <div>
      <span className='timespan'>{timespan}</span>
      <span className='name'>{name}</span>
      {img}

      {children}
    </div>
  );
};

Experience.propTypes = {
  name: PropTypes.string.isRequired,
  timespan: PropTypes.string,
  imgUrl: PropTypes.oneOfType([PropTypes.string, PropTypes.bool])
};
Experience.defaultProps = {
  timespan: '',
  imgUrl: false
};

const Experiences = () => (
  <section id='experiences'>
    {/* HYPED */}
    <Experience
      name='Project Manager at HYPED'
      timespan='Sep 2019 - Now'
      imgUrl='/images/hyped.jpg'
    >
      <p>
        In 2013 a joint team from Tesla and SpaceX published a{' '}
        <a
          className='link'
          href='https://www.spacex.com/sites/spacex/files/hyperloop_alpha-20130812.pdf'
        >
          paper
        </a>{' '}
        outlining the concept of a so-called &apos;Hyperloop&apos; transport
        system which has the potential of replacing high-speed-railways. To
        encourage development, SpaceX has been hosting a yearly pod competition
        for universities starting in 2017.
      </p>
      <p>
        Within the Edinburgh University Hyperloop team (HYPED) I am leading a
        subteam of ten people responsible for the mission control system that
        and developing an internal debugging tool used by other subteams to test
        their soft- and hardware. The mission control is the system through
        which the pod is accessed while it is on the track, getting data and
        sending controls.
      </p>
      <p>
        Some of the technologies we are working with are network infrastructure,
        web front- &amp; backend and controlling unix processes (IPC).{' '}
        <a href='https://github.com/hyp-ed' className='link'>
          – to the repo
        </a>
      </p>
    </Experience>

    {/* build-yours */}
    <Experience
      name='Founder of build&#8209;yours.com' // non-breaking hyphen: &#8209;
      timespan='Jan 2017 - Dec 2019'
      imgUrl='/images/wordclock.jpg'
    >
      <p>
        Together with my brother I used to work on several projects involving
        both, hard- and software. We then decided to develop DIY kits, which
        turned into build&#8209;yours.
      </p>
      <p>
        In the three years that I worked on build&#8209;yours, we developed from
        a company selling only kits to selling finished goods. With
        technological advances over many product iterations we managed to build
        a solid customer base. The primary business of build-yours is selling
        premium ‘smart’ wall clocks that tell the time in words.
      </p>
      <p>
        Some of the technologies involved in developing our online-shop, the
        software running on the products as well as the complimentary services
        are Lua running on nodeMCUs, JS in front- and backend, PHP and MySQL.
      </p>
    </Experience>

    {/* Mathe-Talentfoerder */}
    <Experience
      name='Coach for advanced group of young mathmaticians'
      timespan='Apr 2015 - Dec 2018'
      imgUrl='/images/maths.png'
    >
      <p>
        As many smart students aren&apos;t challenged enough in school, they aim
        to meet their curiosity outside of school. Out of this demand, a group
        arose to connect and foster youngsters with a special interest in
        mathematics. After taking part in workshops organised by this group,
        together with a friend I decided to organise a semi-weekly mathematics
        workshop teaching interesting concepts that go beyond what is taught in
        school.
      </p>
    </Experience>

    {/* DroPS */}
    <Experience
      name='Microgravity research project at DLR &amp; ZARM'
      timespan='Jan - June 2018'
      imgUrl='/images/drops.jpg'
    >
      <p>
        Together with a group of five others I worked on a research project in
        physics to find out how certain mechanical principles behave in
        micro-gravity. We got to test our hypothesis and collect data at the
        ZARM drop tower in Bremen.
      </p>
    </Experience>
  </section>
);

export default Experiences;
