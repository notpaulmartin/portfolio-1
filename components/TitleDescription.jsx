/*
 * File:          TitleDescription.jsx
 * Project:       portfolio
 * File Created:  Sunday, 5th January 2020 2:20:09 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Tuesday, 7th January 2020 5:33:04 pm
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';
import TypeIt from 'typeit';

const descriptions = [
  'Developer',
  'Entrepreneur',
  'Mentor',
  'University student',
  'Passionate runner'
  // 'Adventurous'
];

class TypedDescription extends React.Component {
  componentDidMount() {
    const typer = new TypeIt(this.el, {
      loop: true,
      speed: 50,
      deleteSpeed: 25,
      breakLines: false,
      cursorChar: ''
    });

    typer.empty();

    function write(string) {
      typer.type(string);
      typer.pause(2500 * Math.log10(1 + string.length)); // the longer the text, the longer it takes to read it
      typer.delete(string.length);
      typer.pause(800); // wait before writing next string
    }

    descriptions.map(write);
    typer.go();
  }

  render() {
    return (
      <span
        id='typed_description'
        ref={el => {
          this.el = el;
        }}
      />
    );
  }
}

export default TypedDescription;
