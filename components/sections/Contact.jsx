/*
 * File:          Contact.jsx
 * Project:       portfolio
 * File Created:  Monday, 6th January 2020 6:26:28 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Sunday, 12th January 2020 8:23:21 pm
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';

const Social = ({ name, iconSrc, href = '#', className }) => {
  let icon;
  if (iconSrc) icon = <img alt={name} src={iconSrc} />;
  else icon = '';

  return (
    <div className='social'>
      {icon}
      <a href={href} className={`${className} link`}>
        {name}
      </a>
    </div>
  );
};

const Contact = () => (
  <section id='contact'>
    <p>
      I’m always looking to meet new interesting People, so come and reach out
      to me on…
    </p>
    <Social name='GitHub' href='https://github.com/notpaulmartin' />
    <Social name='LinkedIn' href='https://linkedin.com/in/notpaulmartin' />
    <Social name='Twitter' href='https://twitter.com/notpaulmartin' />
    <Social name='Instagram' href='https://instagram.com/notpaulmartin' />
    <Social name='Or plain old email' href='mailto:paul@blibspace.com' />
  </section>
);

export default Contact;
