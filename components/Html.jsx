/*
 * File:          Html.jsx
 * Project:       portfolio
 * File Created:  Sunday, 5th January 2020 10:28:21 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Sunday, 12th January 2020 3:47:28 pm
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';

import Head from 'next/head';

export const CustomHead = () => (
  <Head>
    <meta name='Description' content='' />
    <title>Paul Martin</title>
  </Head>
);

export const Bottom = () => (
  <div>
    <script src='/frontend.js' />
  </div>
);

export default CustomHead;
