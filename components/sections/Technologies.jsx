/*
 * File:          Technologies.jsx
 * Project:       portfolio
 * File Created:  Monday, 6th January 2020 6:26:28 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Saturday, 11th January 2020 1:22:52 pm
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';

const Technology = ({ name, percent, bold: isBold }) => {
  let bold;
  if (isBold) bold = 'bold';
  else bold = '';

  return (
    <div className='technology'>
      <div className='bar'>
        <div className='bar_line' />
        <div className='bar_fill' style={{ width: `${percent}%` }} />
      </div>
      <span className={`name ${bold}`}>{name}</span>
    </div>
  );
};

const Technologies = () => (
  <section id='technologies'>
    <Technology name='nodeJS' percent='95' bold />
    <Technology name='Lua (on nodeMCU)' percent='85' bold />
    <Technology name='Vanilla JavaScript (Web)' percent='80' />
    <Technology name='PHP' percent='80' />
    <Technology name='MySQL' percent='60' />
    <Technology name='Haskell' percent='60' />
    <Technology name='React &amp; next.js' percent='50' />
    <Technology name='Python' percent='40' />
  </section>
);

export default Technologies;
