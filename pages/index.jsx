/*
 * File:          index.jsx
 * Project:       portfolio
 * File Created:  Sunday, 5th January 2020 2:20:09 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Sunday, 12th January 2020 2:41:45 pm
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';

import '../styles/style.sass';

import { CustomHead, Bottom } from '../components/Html';
import { Navbar } from '../components/Navbar';
import Footer from '../components/Footer';
import TypedDescription from '../components/TitleDescription';
import PicturePaul from '../components/Images';

import About from '../components/sections/About';
import Experiences from '../components/sections/Experiences';
import Technologies from '../components/sections/Technologies';
import Projects from '../components/sections/Projects';
import Contact from '../components/sections/Contact';

function Page() {
  return (
    <div>
      <CustomHead />
      <Navbar />
      <PicturePaul />
      <div className='title'>
        <h1 className='name'>
          <span>Paul Martin</span>
        </h1>
        <TypedDescription />
      </div>
      <main>
        <div className='h2'>About myself</div>
        <About />

        <div className='h2'>Some things I&apos;ve worked on</div>
        <Experiences />

        <div className='h2'>Technologies I&apos;m comfortable with</div>
        <Technologies />

        <div className='h2'>Other open-source projects</div>
        <Projects />

        <div className='h2'>Let&apos;s connect!</div>
        <Contact />

        <Footer />
      </main>
      <Bottom />
    </div>
  );
}

export default Page;
