/*
 * File:          Navbar.jsx
 * Project:       portfolio
 * File Created:  Sunday, 5th January 2020 10:28:21 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Sunday, 12th January 2020 2:51:30 pm
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-scroll';

export const ScrollLink = ({ to, className, children }) => (
  <Link
    className={`${className} link`}
    to={to}
    smooth
    duration={500}
    offset={-200}
  >
    {children}
  </Link>
);

ScrollLink.propTypes = {
  to: PropTypes.string.isRequired
};

export const Navbar = () => (
  <div id='navbar'>
    <ScrollLink className='navbar-item' to='about'>
      About
    </ScrollLink>

    <ScrollLink className='navbar-item' to='experiences'>
      Experience
    </ScrollLink>

    <ScrollLink className='navbar-item' to='technology'>
      Skills
    </ScrollLink>

    <ScrollLink className='navbar-item' to='projects'>
      Projects
    </ScrollLink>

    <ScrollLink className='navbar-item' to='contact'>
      Contact
    </ScrollLink>
  </div>
);

export default Navbar;
