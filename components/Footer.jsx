/*
 * File:          Footer.jsx
 * Project:       portfolio
 * File Created:  Friday, 10th January 2020 11:11:39 am
 * Author(s):     Paul Martin
 *
 * Last Modified: Friday, 10th January 2020 11:24:21 am
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';

const Footer = () => {
  const year = new Date().getFullYear();
  return <section id='footer'>© {year} - Paul Martin</section>;
};

export default Footer;
