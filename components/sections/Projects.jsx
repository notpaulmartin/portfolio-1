/*
 * File:          Projects.jsx
 * Project:       portfolio
 * File Created:  Monday, 6th January 2020 6:26:28 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Sunday, 12th January 2020 8:12:40 pm
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';
import PropTypes from 'prop-types';
import inView from 'in-view';

class Project extends React.Component {
  constructor(props) {
    super(props);
    const { imgUrl, name, videoSrc } = props;

    // if isset(imgUrl) -> display image
    // else if isset(videoSrc) -> display video
    // else -> don't display any media

    if (imgUrl) this.media = <img alt={name} src={imgUrl} />;
    else if (videoSrc)
      this.media = (
        <video autoPlay loop muted>
          <source src={videoSrc} />
        </video>
      );
    else this.media = '';
  }

  componentDidMount() {
    document.querySelector('video').onclick = ({ target }) => target.play();
  }

  render() {
    return (
      <div>
        <span className='name'>{this.props.name}</span>
        <span className='technology'>{this.props.technology}</span>
        {this.media}

        {this.props.children}
      </div>
    );
  }
}

Project.propTypes = {
  name: PropTypes.string.isRequired,
  technology: PropTypes.string,
  imgUrl: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  videoSrc: PropTypes.oneOfType([PropTypes.string, PropTypes.bool])
};
Project.defaultProps = {
  technology: '',
  imgUrl: false,
  videoSrc: false
};

const Projects = () => (
  <section id='projects'>
    {/* Autoflash */}
    <Project
      name='Autoflash'
      technology='JS -> nodeJS'
      videoSrc='/videos/autoflash.mp4'
    >
      <p>
        In the process of working on build-yours.com I developed several
        internal software to aid with certain tasks. Since our products include
        embedded chips (ESP8266) which we need to program somehow, I created
        autoflash. In its third iteration, which I have now made public,
        uploading to a chip takes a mere 15s as oppose to the 2min it initially
        took us to manually upload the firmware and every single file of code.
      </p>
      <p>
        An advantage this tool has is not only the upload speed but also that it
        automatically selects the chip&apos;s port.
        <a
          className='link'
          href='https://github.com/notpaulmartin/autoflash'
          style={{ whiteSpace: 'nowrap' }}
        >
          {' '}
          – to source
        </a>
      </p>
    </Project>

    {/* PDFme */}
    <Project name='PDFme' technology='JS -> Web (Chrome extension)'>
      <p>
        PDFme is a Chrome extension to convert a website to PDF, splitting it
        into individual A4 pages. What you see is what you get!
      </p>
      <p>
        If you have ever tried printing a webpage or converting it to a
        multipage PDF, you&apos;ve encountered this issue: More often than not
        the print dialogue completely massacres the entire website.
      </p>
      <p>
        But how can it be that in a world of self-driving cars and quantum
        computers, you still can&apos;t properly print websites?
      </p>
      <p>
        This is why, together with my good friend Alexandra, I decided to tackle
        this very problem and develop a simple Chrome extension that enables the
        printing of webpages as is.
        <a
          className='link'
          href='https://github.com/notpaulmartin/PDFme'
          style={{ whiteSpace: 'nowrap' }}
        >
          {' '}
          – to source
        </a>
      </p>
    </Project>
  </section>
);

export default Projects;
