/*
 * File:          About.jsx
 * Project:       portfolio
 * File Created:  Monday, 6th January 2020 6:26:28 pm
 * Author(s):     Paul Martin
 *
 * Last Modified: Sunday, 12th January 2020 3:50:42 pm
 * Modified By:   Paul Martin (paul@blibspace.com)
 */

import React from 'react';

import { ScrollLink } from '../Navbar';

const About = () => (
  <section id='about'>
    <p>
      Thanks for stopping by! I&apos;m Paul, a BEng Computer Science (Hons)
      student at The University of Edinburgh. Even though I currently enjoy
      living in the UK, I&apos;m originally from the north coast of Germany,
      being born in the windy town of Wilhelmshaven and growing up an hour
      south, in Bremen.
    </p>
    <p>
      I&apos;ve always been fascinated by how a series of very simple components
      can be combined to create complex systems. Whether it&apos;s Lego bricks,
      circuits, or software. I must admit though that my interest in Lego fell
      by the wayside.
    </p>
    <p>
      While I&apos;ve played around with most popular languages out there, a
      list of the technologies I&apos;m comfortable working with can be found in
      the <ScrollLink to='technologies'>skills</ScrollLink> section. If given a
      choice, I&apos;d go probably go for JavaScript more often than not.
    </p>
    <p>
      When I&apos;m not either studying or hacking on new projects, you&apos;ll
      probably find me running around town, traveling the world, taking and
      editing photos or doing all three things simultaneously.
    </p>
  </section>
);

export default About;
